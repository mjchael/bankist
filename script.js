'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
    owner: 'Michael Ra',
    movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
    interestRate: 1.2, // %
    pin: 1111,

    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
        '2020-05-27T17:01:17.194Z',
        '2020-07-11T23:36:17.929Z',
        '2021-06-12T10:51:36.790Z',
    ],
    currency: 'EUR',
    locale: 'pt-PT', // de-DE
};

const account2 = {
    owner: 'Jessica Davis',
    movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
    interestRate: 1.5,
    pin: 2222,

    movementsDates: [
        '2019-11-01T13:15:33.035Z',
        '2019-11-30T09:48:16.867Z',
        '2019-12-25T06:04:23.907Z',
        '2020-01-25T14:18:46.235Z',
        '2020-02-05T16:33:06.386Z',
        '2020-04-10T14:43:26.374Z',
        '2020-06-25T18:49:59.371Z',
        '2020-07-26T12:01:20.894Z',
    ],
    currency: 'USD',
    locale: 'en-US',
};


const accounts = [account1, account2];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

const euro = '€';

const formatDate = function(date, locale){
    // const day = date.getDate().toString().padStart(2, "0");
    // const month = (date.getMonth() +1).toString().padStart(2, "0"); // month is zero based
    // const year = date.getFullYear();
    // const min = date.getMinutes().toString().padStart(2, "0");
    // const sec = date.getSeconds().toString().padStart(2, "0");
    // const hour = date.getHours().toString().padStart(2, "0");
    //
    // return `${day}/${month}/${year} ${hour}:${min}:${sec}`;

    const now = date;
    const options = {
        hour: 'numeric',
        minute: 'numeric',
        day: 'numeric',
        month: 'numeric', // 'long' // '2-digit' // 'numeric'
        year: 'numeric',
        // weekday: 'long'
    };

    // const navigatorLanguage = navigator.language;
    const formatter = new Intl.DateTimeFormat(locale, options)
    return formatter.format(now);
};

const daysPassed = function(day1, day2, locale){
    const dayPassed =  Math.floor(Math.abs(day1 - day2)/(1000 * 60 * 60 * 24));

    return dayPassed === 0 ? 'Today' : ( dayPassed === 1 ? 'Yesterday': (dayPassed<=7?`${dayPassed} days ago`:formatDate(day2, locale)));
};

const displayMovements = function (account, formatter, sort = false) {
  containerMovements.innerHTML = "";
  const movementsCopy = account.movements.slice();

  if (sort) movementsCopy.sort((a,b) => a-b);

  movementsCopy.forEach ( (mov, i ) => {
    const type = mov>0? 'deposit':'withdrawal';
    const html = `
        <div class="movements__row">
          <div class="movements__type movements__type--${type}">${i + 1} ${type} </div>
          <div class="movements__date">${daysPassed(new Date() , new Date(account.movementsDates[i]), account.locale)}</div>
          <div class="movements__value">${formatter.format(mov)}</div>
        </div>`;
    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

const displayBalance = function(account, formatter){
  account.balance = account.movements.reduce((acc, curr) => acc+= curr, 0);
  labelBalance.innerHTML =  `${formatter.format(account.balance)}`;
};

const displaySummary = function(account, formatter){
  const deposits = account.movements
      .filter(mov => mov > 0)
      .reduce((acc, curr) => acc+=curr, 0);
  const withdrawals = account.movements
      .filter(mov => mov < 0)
      .reduce((acc, curr) => acc+=curr, 0);
  const interest = account.movements
      .filter(mov => mov>0)
      .map(a => a * account.interestRate/100)
      .filter(a => a >= 1)
      .reduce((acc, curr) => acc+=curr, 0);
  labelSumIn.innerHTML=`${formatter.format(deposits)}`;
  labelSumOut.innerHTML=`${formatter.format(withdrawals)}`;
  labelSumInterest.innerHTML = `${formatter.format(interest)}`;
};

const computeUserName = function(userName) {
  return userName.toLowerCase().split(' ')
      .map((name) => name.substr(0,1))
      .reduce((a, b) =>a.concat(b));
};

const createUserNames = function(accounts) {
  accounts.forEach( acc => acc['username'] = computeUserName(acc.owner));
};

const updateUI = function(account){
  const options = {
    style: 'currency',
    currency: account.currency
  };

  const formatter = new Intl.NumberFormat(account.locale, options)

  // display movements
  displayMovements(account, formatter);

  // display balance
  displayBalance(account, formatter);

  // display summary
  displaySummary(account, formatter);
};

const logOut = function(){
    containerApp.style.opacity = 0;
    currentAccount = null;
    labelWelcome.innerHTML = `Log in to get started`;
};


createUserNames(accounts);
// Event Handlers
let currentAccount;
// //fake login
// currentAccount = account1;
// updateUI(currentAccount);
// containerApp.style.opacity = 100;
// // end fake login

let timeInterval;
let timerLogoutInterval;

const startLogOutTimer = function () {
    const options = {
        minute: 'numeric',
        second: 'numeric'
    };

    const formatter = new Intl.DateTimeFormat(currentAccount.locale, options)

    const loggedInTime = new Date();
    const logOutTime =  new Date(loggedInTime.getTime() + 5*60000);

    const calcTime = function (logOutTime){
        const remTime = logOutTime - new Date();
        if (remTime >0){
            return remTime;
        }
        return 0;
    };

    if (timeInterval){
        clearInterval(timeInterval);
    }
    if (timerLogoutInterval){
        clearInterval(timerLogoutInterval);
    }

    timeInterval = setInterval(
        () =>
            labelTimer.innerHTML =
                formatter.format(calcTime(logOutTime)),
        1000
    );

    timerLogoutInterval = setInterval(
        () => {
            if (logOutTime - new Date()<=0){
                clearInterval(timeInterval);
                logOut();
                clearInterval(timerLogoutInterval);
            }
        },
        1000
    );
};

btnLogin.addEventListener(
    'click', function(e){
      e.preventDefault();
      const username = inputLoginUsername.value;
      const pin = Number(inputLoginPin.value);

      currentAccount = accounts.find(account => account.username === username && account.pin === pin);
      if (currentAccount){
        // display ui and message
        labelWelcome.innerHTML = `Welcome back, ${currentAccount.owner.split(' ')[0]}`;
        containerApp.style.opacity = 100;

        //clear input fields
        inputLoginUsername.value = '';
        inputLoginPin.value = '';
        inputLoginPin.blur();
        labelDate.innerHTML = formatDate(new Date(), currentAccount.locale);

        updateUI(currentAccount);
        startLogOutTimer();


      }else {
        console.log('Failed');
      }
    }
);

btnTransfer.addEventListener(
  'click', function(e){
      e.preventDefault();
      const amount = Number(inputTransferAmount.value);

      const receiverAccount = accounts.find(account => account.username ===  inputTransferTo.value);


      inputTransferAmount.value = '';
      inputTransferTo.value = '';

      if (receiverAccount){
        if (amount > 0 && currentAccount.balance >=amount && receiverAccount.username !== currentAccount.username) {
          console.log(amount);
          // credit current account
          currentAccount.movements.push(-1 * amount);
          currentAccount.movementsDates.push(new Date().toISOString());
          // debit receiver account
          receiverAccount.movements.push(amount);
          receiverAccount.movementsDates.push(new Date().toISOString());


          updateUI(currentAccount);
          startLogOutTimer();
        }
      }
    }
);

btnLoan.addEventListener(
  'click', function(e){
     e.preventDefault();
     const loanAmount = Math.floor(inputLoanAmount.value);

     if (loanAmount>0){
         const canLoanBeApproved = currentAccount.movements.some(amount => amount > loanAmount * 0.10);
         if (canLoanBeApproved){
             setTimeout( () => {
                     currentAccount.movements.push(loanAmount);
                     currentAccount.movementsDates.push(new Date().toISOString())
                     updateUI(currentAccount);
                 }, 2500
             );
             startLogOutTimer();
         }
     }
     inputLoanAmount.value = '';
    }
);

let sorted = false;
btnSort.addEventListener(
  'click', function(e){
        e.preventDefault();
        const options = {
            style: 'currency',
            currency: currentAccount.currency
        };

        const formatter = new Intl.NumberFormat(currentAccount.locale, options)

        displayMovements(currentAccount, formatter, sorted= !sorted);
        startLogOutTimer();
    }
);

btnClose.addEventListener(
    'click', function(e) {
      e.preventDefault();

      if (inputCloseUsername.value === currentAccount.username){
          const accountToDeleteIndex =  accounts.findIndex(
              account => account.username === inputCloseUsername.value && account.pin === Number(inputClosePin.value));
          if (accountToDeleteIndex > 0){
              accounts.splice(accountToDeleteIndex, 1);
              inputCloseUsername.value = '';
              inputClosePin.value = '';
              logOut();
          }
      }
    }
);
