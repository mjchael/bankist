# bankist

Practice work done when following the course [the-complete-javascript-course](https://www.udemy.com/course/the-complete-javascript-course/) on udemy.

If you are new to javascript or want to grasp all the key concept of  javascript, do buy the course. 

## Demo
Check the project [here](https://mjchael.gitlab.io/bankist/).
